# webdriverio-pmallo

En este proyecto se puede ver la automatizacion de un caso de prueba en Android Browser y PC Browser

## Intalacion

Descargar el proyecto

Ejecutar el siguiente comando para instalar las dependencias el proyecto:

`npm install`

## Ejecucion en Android Browser

Se debe enviar 2 variables en la linea de comando o declararlas como veriables de entorno


`DEVICE_NAME = (Nombre del dispositivo)`

`PLATFORM_VERSION = (Version de Android del dispositivo)`


y el comando para ejecutar las pruebas en el caso que se declaren estas propiedades como variables de entorno es:

`npm run android`

sino podemos enviar las propiedades en la linea de ejecucion

`DEVICE_NAME=emulator-5554 PLATFORM_VERSION=7.0 npm run android`

## Ejecucion en PC Browser

Se debe ejecutar el siguiente comando:

`npm run web`

# Informacion del proyecto

Se utilizo las siguientes tools

'APPIUM'
'CUCUMBER'
'CHROMEDRIVER'
'Multiple Cucumber HTML Reporter'

Lo particular de este poroyecto es que utiliza el mismo casos con los mismos pasos para 2 plataformas ditintas