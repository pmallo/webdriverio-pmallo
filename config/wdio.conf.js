const { generate } = require('multiple-cucumber-html-reporter');
const { removeSync } = require('fs-extra');

exports.config = {
   
    runner: 'local',
    sync: true,
    
   

    specs: [
        './src/test/*.feature'
    ],
    
    exclude: [],
    
    logLevel: 'info',
    
    bail: 0,
    
    waitforTimeout: 10000,
    
    connectionRetryTimeout: 120000,
    
    connectionRetryCount: 3,
    
    port: 4723,

    
    
    framework: 'cucumber',
    cucumberOpts: {
        backtrace: false,
        requireModule: [],
        failAmbiguousDefinitions: false,
        failFast: false,
        ignoreUndefinedDefinitions: false,
        name: [],
        profile: [],
        
        snippetSyntax: undefined,
        snippets: true,
        source: true,
        strict: false,
        tagsInTitle: false,
        timeout: 50000,
        retry: 0
    },
    
    reporters: [
        [
            'cucumberjs-json', {
                jsonFolder: './reports/json',
                language: 'es',
            }
        ]
    ],
    
    onPrepare: () => {
        removeSync('./reports/');
      },

    onComplete: function(exitCode, config, capabilities, results) {
        generate({
            jsonDir: './reports/json',
            reportPath: './reports/html',
            openReportInBrowser: true,
            
        });
    },
    
}
