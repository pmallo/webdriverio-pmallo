module.exports = class Properties {
    
    static deviceName() {
        return process.env.DEVICE_NAME;
    }

    static platFormVersion() {
        return process.env.PLATFORM_VERSION;
        
    }
    
    static urlEbay() {
        return 'www.ebay.com';
    }

    static chromeVersion() {
        return process.env.npm_package_devDependencies_chromedriver;
    }

    static pcOSName() {
        return process.env.DESKTOP_SESSION;
    }

    static pcDeviceName() {
        return process.env.USERNAME;
    }

    
    
}
