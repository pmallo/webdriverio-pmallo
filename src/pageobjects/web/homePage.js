const basePage = require('./basePage');
const properties = require('../../info/properties.js')

class homePage extends basePage {
   
    get inputFind () { return $('#gh-ac') }
    get btnSubmit () { return $('#gh-btn') }


    
    search (product) {
        this.inputFind.setValue(product);
        this.btnSubmit.click(); 
    }

  
    open () {
        return super.open(properties.urlEbay());
    }

    pageIsLoaded() {
        return this.inputFind.isDisplayed()
      }
}

module.exports = homePage;