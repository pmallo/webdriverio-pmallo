const { Then } = require('cucumber');
const expectChai = require('chai').expect;

const resultPage = new (require('../../pageobjects/web/resultPage'));

Then(/^I see find results$/, function () {
    var result = resultPage.getResult()
    expectChai(result).not.null
    console.log(result)
});

