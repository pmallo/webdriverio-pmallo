const BasePage = require('./BasePage');


class HomePage extends BasePage {
   
    get inputFind () { return $('#gh-ac') }
    get btnSubmit () { return $('#gh-btn') }


    
    search (product) {
        this.inputFind.setValue(product);
        this.btnSubmit.click(); 
    }

  
    open () {
        return super.open('http://www.ebay.com');
    }
}

module.exports = new HomePage();
