
class ResultPage {
   
    
    get resultField () { return $('//div[@class="srp-controls__row-cells"]//h1/span[1]') }
    get btnSubmit () { return $('button[type="submit"]') }

    
    getResult () {
        return this.resultField.getText()
    }

  
}

module.exports = new ResultPage();
